"""
Very simple demonstration of the @property annotations in Python 3.

Demonstrates the usage of @property getter and setter functions and adds
some logic to these functions. Also demonstrates the use of a setter function
for multiple properties using keyworded arguments.
"""

class Person:
    """
    Example person class.
    """
    def __init__(self, name, age):
        """
        Constructor function.
        
        Note: calls setter functions defined below to perform checks!
        
        Args:
            name    String holding the person's name.
            age     Integer holding the person's age.
        """
        # Set initial values
        self.name = name
        self.age  = age

    @property
    def name(self):
        """
        Getter function for name.
        
        Returns:
            Person's name as a string.
        """
        return self._name
    
    @name.setter
    def name(self, name):
        """
        Setter function for name.
        
        Args:
            name    Person's name as a string.
        """
        # Check if name is string or unicode type
        if type(name).__name__ not in ('str', 'unicode'):
            raise ValueError('Name is not a string!')
        
        # Strip white space
        name = name.strip()
        
        # Check length
        if len(name) > 0:
            self._name = name
        else:
            raise ValueError('Name contains zero characters!')
        
    @property
    def age(self):
        """
        Getter function for age.
        
        Returns:
            Person's age as an integer.
        """
        return self._age
    
    @age.setter
    def age(self, age):
        """
        Setter function for age.
        
        Args:
            age     person's age as integer.
        """ 
        # Try casting to make sure age is an integer
        try:
            age = int(age)
        except ValueError:
            raise ValueError('Age is not an integer!')
        
        # Check age range
        if 0 < age < 120:
            self._age = age
        else:
            raise ValueError('Age outside of valid range (0-120)!')
   
    def set(self, **kwargs):
        """
        Setter function using keyworder arguments.
        
        Note: uses our previously defined setter functions!
        
        Args:
            **kwargs    Keyworded arguments, property => value pairs
        """
        # Check for age in arguments
        if 'age' in kwargs:
            # Set person's age
            self.age = kwargs['age']
        
        # Check for name in arguments
        if 'name' in kwargs:
            # Set name
            self.name = kwargs['name']
    
    def __str__(self):
        """
        Convert Person oblect to printable string.
        
        Note: calls previously defined getter functions!
        
        Returns:
            Person object as string
        """
        return 'Name: %s --- Age: %d' % (self.name.title(), self.age)
        

def main():
    """
    Main program; demonstrates the use of getter and setter functions by
    creating Person objects.
    """
    
    # Create a person and print
    # Calls constructor, which in turn calls setter functions
    jd = Person('John Doe', 31)
    print(jd)

    # Update name and age, calls setter functions
    jd.name = 'jane doe'
    jd.age  = '25'          # Works because we can cast to integer
    print(jd)

    # Cannot set invalid age
    try:
        jd.age = 130
    except ValueError as e:
        print('Cannot set age to 130: %s' % e)
        
    # Cannot set invalid name
    try:
        jd.name = 1234
    except ValueError as e:
        print('Cannot set name to 1234: %s' % e)

    # Use set function to update multiple properties
    jd.set(name = 'John Doe', age = 31)
    print(jd)

    
# Call main routine
if __name__ == '__main__':
    main()